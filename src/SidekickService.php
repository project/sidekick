<?php

namespace Drupal\sidekick;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Renderer;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions;

/**
 * Service for HTTP request.
 */
class SidekickService
{

    /**
     * The HTTP client.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $http_client;

    /**
     * The current user.
     *
     * @var \Drupal\Core\Session\AccountInterface
     */
    protected $currentUser;

    /**
     * The sidekick config.
     *
     * @var \Drupal\Core\Config\Config
     */
    protected $configFactory;

    /**
     * Language manager.
     *
     * @var \Drupal\Core\Language\LanguageManagerInterface
     */
    protected $languageManager;

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Renderer.
     *
     * @var \Drupal\Core\Render\Renderer
     */
    protected $renderer;

    /**
     * Renderer.
     *
     * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface;
     */
    protected $loggerFactory;

    /**
     * CacheFactory.
     *
     * @var \Drupal\Core\Cache\CacheBackendInterface;
     */
    protected $cacheFactory;

    /**
     * The time service.
     *
     * @var \Drupal\Component\Datetime\TimeInterface
     */
    protected $time;

    /**
     * @var string
     */
    protected $keyStatusApi;

    /**
     * @var string
     */
    protected $accountApi;

    /**
     * @var string
     */
    protected $modulesListApi;

    /**
     * @var string
     */
    protected $languagesListApi;

    /**
     * @var string
     */
    protected $writingStyleListApi;

    /**
     * @var string
     */
    protected $chatApi;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @param \GuzzleHttp\ClientInterface $http_client
     * The Http client
     * @param \Drupal\Core\Session\AccountInterface $currentUser
     * The current user.
     * @param \Drupal\Core\Config\ConfigFactory $configFactory
     * The configuration factory.
     * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
     * LanguageManager service.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     * The entity type manager.
     * @param \Drupal\Core\Render\Renderer $renderer
     * Renderer service.
     * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $factory
     * LoggerChannel.
     * @param \Drupal\Core\Cache\CacheBackendInterface $cacheFactory
     * CacheFactory.
     */
    public function __construct(
        ClientInterface               $http_client,
        AccountInterface              $currentUser,
        ConfigFactory                 $configFactory,
        LanguageManagerInterface      $languageManager,
        EntityTypeManagerInterface    $entity_type_manager,
        Renderer                      $renderer,
        LoggerChannelFactoryInterface $factory,
        CacheBackendInterface         $cacheFactory,
        TimeInterface                 $time
    )
    {
        $this->http_client = $http_client;
        $this->currentUser = $currentUser;
        $this->configFactory = $configFactory;
        $this->languageManager = $languageManager;
        $this->entityTypeManager = $entity_type_manager;
        $this->renderer = $renderer;
        $this->loggerFactory = $factory;
        $this->cacheFactory = $cacheFactory;
        $this->keyStatusApi = 'https://assistant.ai-sidekick.app/api/v1/apikey-status';
        $this->accountApi = 'https://assistant.ai-sidekick.app/api/v1/accounts/current';
        $this->modulesListApi = 'https://assistant.ai-sidekick.app/api/v1/modules';
        $this->languagesListApi = 'https://assistant.ai-sidekick.app/api/v1/languages';
        $this->writingStyleListApi = 'https://assistant.ai-sidekick.app/api/v1/writing-styles';
        $this->chatApi = 'https://assistant.ai-sidekick.app/api/v1/chat';
        $this->apiKey = $this->configFactory->get('sidekick.settings')->get(
            'api_key'
        );
        $this->time = $time;
    }

    /**
     * @param $language_code
     * @param string|null $apiKey
     * @param bool|null $reset_cache
     * @return array
     * @throws GuzzleException
     */

    public function checkKeyStatus($language_code, ?string $apiKey = '', ?bool $reset_cache = FALSE)
    {
        if (empty($apiKey)) {
            $apiKey = $this->apiKey;
        }
        if (empty($apiKey)) {
            $build_message['message'] = [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => t(
                    'You haven\'t added your Sidekick api key. Please add it.'
                ),
            ];
            $build_message['link'] = [
                '#type' => 'link',
                '#title' => t('here'),
                '#url' => Url::fromRoute('sidekick.settings_form'),
            ];
            $message = \Drupal::service('renderer')->render($build_message);
            \Drupal::messenger()->addWarning($message);
            return [];
        }


        $data = $this->cacheFactory->get('key_status');
        if (!$reset_cache && $data) {
            $keyStatus = $data->data;
        } else {
            try {
                $response = $this->http_client->post($this->keyStatusApi, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'body' => json_encode([
                        'token' => $apiKey,
                        'language' => $language_code,
                    ]),
                ]);
                $keyStatus = [
                    'status' => $response->getStatusCode(),
                    'message' => json_decode($response->getBody()),
                ];

                $this->cacheFactory->set(
                    'key_status',
                    $keyStatus,
                    $this->time->getRequestTime() + 86400
                ); // Set Cache for day
            } catch (ClientException $e) {
                $keyStatus = [
                    'status' => $e->getCode(),
                    'message' => json_decode($e->getResponse()->getBody()->getContents())->message,
                ];
            }
        }
        return $keyStatus;
    }

    /**
     * @param $language_code
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getModules($language_code)
    {
        $data = $this->cacheFactory->get('module_list');
        if ($data) {
            $modules = $data->data;
        } else {
            try {
                $response = $this->http_client->get($this->modulesListApi, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->apiKey,
                    ],
                    'body' => json_encode([
                        'language' => $language_code,
                    ]),
                ]);
                $modules = [
                    'status' => $response->getStatusCode(),
                    'message' => json_decode($response->getBody()),
                ];

                $this->cacheFactory->set(
                    'module_list',
                    $modules,
                    $this->time->getRequestTime() + 86400
                ); // Set Cache for day

            } catch (ClientException $e) {
                $modules = [
                    'status' => $e->getCode(),
                    'message' => json_decode($e->getResponse()->getBody()->getContents())->message,
                ];
            }
        }
        return $modules;
    }

    /**
     * @param $language_code
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLanguages($language_code)
    {
        try {
            if ($cache = $this->cacheFactory->get('language_list')) {
                return $cache->data;
            } else {
                $response = $this->http_client->get($this->languagesListApi, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->apiKey,
                    ],
                    'body' => json_encode([
                        'language' => $language_code,
                    ]),
                ]);

                $data = [
                    'status' => $response->getStatusCode(),
                    'message' => json_decode($response->getBody()),
                ];

                $this->cacheFactory->set(
                    'language_list',
                    $data,
                    $this->time->getRequestTime() + 86400
                ); // Set Cache for day

                return $data;
            }
        } catch (ClientException|RequestException|TransferException|BadResponseException $exception) {
            $this->loggerFactory->get('sidekick')->error($exception);

            return (object)[
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $language_code
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWritingStyles($language_code)
    {
        try {
            if ($cache = $this->cacheFactory->get('writing_style')) {
                return $cache->data;
            } else {
                $response = $this->http_client->get(
                    $this->writingStyleListApi,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . $this->apiKey,
                        ],
                        'body' => json_encode([
                            'language' => $language_code,
                        ]),
                    ]
                );

                $data = [
                    'status' => $response->getStatusCode(),
                    'message' => json_decode($response->getBody()),
                ];

                $this->cacheFactory->set(
                    'writing_style',
                    $data,
                    $this->time->getRequestTime() + 86400
                ); // Set Cache for day

                return $data;
            }
        } catch (ClientException|RequestException|TransferException|BadResponseException $exception) {
            $this->loggerFactory->get('sidekick')->error($exception);

            return (object)[
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $sidekick_module
     * @param $language_code
     * @param $writing_style
     * @param $salutation
     * @param $user_input
     * @return array|mixed
     * @throws GuzzleException
     */
    public function getChat(
        $sidekick_module,
        $language_code,
        $writing_style,
        $salutation,
        $user_input
    )
    {
        try {
            $response = $this->http_client->post($this->chatApi, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->apiKey,
                ],
                'query' => ['language' => $language_code],
                'body' => json_encode([
                    'module' => 'page_title_writer',
                    //'module' => $sidekick_module ?? 'page_title_writer',
                    'writing_style' => $writing_style,
                    'salutation' => $salutation,
                    'user_input' => $user_input
                ]),
            ]);

            return [
                'status' => $response->getStatusCode(),
                'message' => json_decode($response->getBody()),
            ];
        } catch (ClientException|RequestException|TransferException|BadResponseException $exception) {
            $this->loggerFactory->get('sidekick')->error($exception);

            return [
                'status' => $exception->getCode(),
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * @param $apiKey
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAccountContext($apiKey)
    {
        try {
            $response = $this->http_client->get($this->accountApi, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $apiKey,
                ]
            ]);

            return [
                'status' => $response->getStatusCode(),
                'message' => Json::decode($response->getBody()),
            ];
        } catch (ClientException|RequestException|TransferException|BadResponseException $exception) {
            $this->loggerFactory->get('sidekick')->error($exception);

            return (object)[
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $apiKey
     * @param $page_briefing
     * @param $target_audience
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setAccountContext($apiKey, $page_briefing, $target_audience)
    {
        try {
            $request_options = [];
            $request_options[RequestOptions::HEADERS] = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $apiKey
            ];
            $request_options[RequestOptions::BODY] = Json::encode([
                'context' => $page_briefing,
                'target_audience' => $target_audience,
                "process_web_page_context" => true
            ]);
            $response = $this->http_client->request(
                'put',
                $this->accountApi,
                $request_options
            );

            return [
                'status' => $response->getStatusCode(),
                'message' => Json::decode($response->getBody()),
            ];
        } catch (ClientException|RequestException|TransferException|BadResponseException $exception) {
            $this->loggerFactory->get('sidekick')->error($exception);

            return (object)[
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $node
     * @param $form_state
     *
     * @return false|string
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function getSideKickMessagingApiData($node, $form_state)
    {
        $nodeUrl = Url::fromRoute(
            'entity.node.canonical',
            ['node' => $node->id()],
            ['absolute' => true]
        )->toString();

        $entity_type = $form_state->getFormObject()->getEntity()
            ->getEntityTypeId();
        $node = $this->entityTypeManager->getStorage($entity_type)
            ->load($node->id());
        $builder = $this->entityTypeManager->getViewBuilder($entity_type);
        $build = $builder->view($node, 'default');
        $output = $this->renderer->render($build);

        $form_fields = $form_state->getFormObject()->getEntity()
            ->getFields();
        $structuredContent = '';
        foreach ($form_fields as $key => $field) {
            if ($form_state->getFormObject()->getEntity()
                ->get($key)->entity) {
                $structuredContent[$key] = $form_state->getFormObject()
                    ->getEntity()->get($key)->entity;
            } else {
                $structuredContent[$key] = $form_state->getFormObject()
                    ->getEntity()->get($key)->value;
            }
        }

        $sideKickMessagingApiData = [
            'version' => '1.0',
            'eventName' => 'page-changed',
            'data' => [
                'url' => $nodeUrl,
                'title' => $node->getTitle(),
                'content' => $output,
                'structuredContent' => $structuredContent,
                'targetAudience' => 'Tech-savvy people',
                'pageBriefing' => '',
            ],
        ];
        return json_encode($sideKickMessagingApiData);
    }

    /**
     * @param $language_code
     * @return array
     */
    public function getQueryParameter($language_code)
    {
        $domain = Url::fromRoute(
            '<front>',
            [],
            ['absolute' => true]
        )->toString();

        return [
            'contentLanguage' => $language_code,
            'interfaceLanguage' => $language_code,
            'language' => $language_code,
            'userId' => $this->currentUser->getDisplayName(),
            'domain' => $domain,
            'apikey' => $this->configFactory->get('sidekick.settings')->get(
                'api_key'
            ),
            'platform' => 'Drupal',
            'hide_account_context' => true,
            'referral' => 'Acolono',
        ];
    }
}
