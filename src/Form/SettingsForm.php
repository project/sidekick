<?php

namespace Drupal\sidekick\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\sidekick\SidekickService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Sidekick settings.
 */
class SettingsForm extends ConfigFormBase
{

    /**
     * @var \Drupal\Core\Language\LanguageManagerInterface
     */
    protected $language;

    /**
     * @var \Drupal\sidekick\SidekickService
     */
    protected $sidekick_service;

    /**
     * @var \Drupal\Core\Render\Renderer
     */
    protected $renderer;

    /**
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Drupal\Core\Language\LanguageManagerInterface $language
     * @param \Drupal\sidekick\SidekickService $sidekick_service
     * @param \Drupal\Core\Render\Renderer $renderer
     */
    public function __construct(
        ConfigFactoryInterface   $config_factory,
        LanguageManagerInterface $language,
        SidekickService          $sidekick_service,
        Renderer                 $renderer
    )
    {
        $this->setConfigFactory($config_factory);
        $this->language = $language;
        $this->sidekick_service = $sidekick_service;
        $this->renderer = $renderer;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('language_manager'),
            $container->get('sidekick.service'),
            $container->get('renderer')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'sidekick_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['info'] = [
            '#type' => 'html_tag',
            '#tag' => 'h5',
            '#value' => $this->t(
                'Please add https://*.ai-sidekick.app/chat in your trustworthy iframe sources.'
            ),
        ];
        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('API Key'),
            '#default_value' => $this->config('sidekick.settings')
                ->get('api_key'),
            '#required' => true,
        ];
        $form['display_type'] = [
            '#type' => 'select',
            '#options' => [
                'no_display' => $this->t('Don\'t display'),
                'inline' => $this->t('Inline'),
                'inline_closed' => $this->t('Inline (closed)'),
                'modal' => $this->t('Modal'),
            ],
            '#title' => $this->t('Show Sidekick in Sidebar'),
            '#default_value' => $this->config('sidekick.settings')
                ->get('display_type'),
        ];
        $form['target_audience'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Target Audience'),
            '#default_value' => $this->config('sidekick.settings')
                ->get('target_audience'),
            '#rows' => '5'
        ];
        $form['page_briefing'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Page briefing'),
            '#default_value' => $this->config('sidekick.settings')
                ->get('page_briefing'),
            '#rows' => '5'
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $api_key = $form_state->getValue('api_key');
        if (!empty($api_key)) {
            $language_code = $this->language
                ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
                ->getId();
            $sidekickKeyStatus = $this->sidekick_service->checkKeyStatus(
                $language_code,
                $api_key,
                true
            );

            if (!empty($sidekickKeyStatus['status']) && $sidekickKeyStatus['status'] == 498) {
                $form_state->setErrorByName('api_key');
                $build_message = [
                    '#type' => 'container',
                    '#markup' => $sidekickKeyStatus['message'],
                    '#attributes' => ['class' => ['messages--error']],
                ];
                $message = $this->renderer->renderPlain($build_message);
                $this->messenger()->addError($message);
            }
        }

        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $api_key = $form_state->getValue('api_key');
        $page_briefing = $form_state->getValue('page_briefing');
        $target_audience = $form_state->getValue('target_audience');

        if (empty($page_briefing) && empty($target_audience)) {
            $sidekickAccountContext = $this->sidekick_service->getAccountContext(
                $api_key
            );
            $page_briefing = $sidekickAccountContext['message']['data']['context'] ?? '';
            $target_audience = $sidekickAccountContext['message']['data']['target_audience'] ?? '';
        } else {
            $this->sidekick_service->setAccountContext(
                $api_key,
                $page_briefing,
                $target_audience
            );
        }

        $this->config('sidekick.settings')
            ->set('api_key', $form_state->getValue('api_key'))
            ->set('display_type', $form_state->getValue('display_type'))
            ->set('target_audience', $target_audience)
            ->set('page_briefing', $page_briefing)
            ->save();

        parent::submitForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['sidekick.settings'];
    }

}
