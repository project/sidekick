<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

* Version
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

VERSION
-------
Version: 1.0.0


INTRODUCTION
------------

AI Sidekick is your personal writing assistant and AI Service provider.
Use it via a chat widget in your backend or use field widgets which will give you Drupal the power to automatically
create ALT texts for images and magically create perfect metad-escriptions etc.
Currently we support English and German language.

REQUIREMENTS
------------

Drupal ^8.7.7 || ^9 || ^10

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* You can use the free version, or [get more features with a license](https://www.neosidekick.com/en/pricing). To
  configure your license key, go to your Drupal CMS at `/admin/config/services/sidekick`.
* AI Sidekick uses iframes. So, if your system blocks iframes or you have cross-site scripting rules in place, please
  allow the following URLs to interact with your site: ```https://*.ai-sidekick.app```  
  If you use [Drupal Security Kit](https://www.drupal.org/project/seckit), please add the wildcard
  URL ```https://*.ai-sidekick.app``` to the frame-src field.

MAINTAINERS
-----------

Current maintainers:

* Shyamlal Sawhney (Shyam) - https://www.drupal.org/u/shyam-sawhney
* Nico Grienauer (Grienauer) - https://www.drupal.org/u/grienauer

by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com  
www.twitter.com/acolono  
www.drupal.org/acolono-gmbh
