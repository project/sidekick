/**
 * @file
 * Sidekick behaviors.
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    Drupal.behaviors.sidekick = {
        attach: function (context, drupalSettings) {
            $('.sidekick-frame', context).find('iframe').parent().addClass('sidekick-frame__content');
            if (drupalSettings.sidekick) {
                var targetIframe = $('.sidekick-frame iframe', context).content;
                if (!!targetIframe) {
                    targetIframe.contentWindow.postMessage(drupalSettings.sidekick.nodeDetails, '*');
                }
            }

            $('.open-sidekick-modal').click(function () {
                $('.sidekick-iframe-modal').dialog({
                    modal: true,
                    width: '80vw'
                });
            })
        },
    };

    Drupal.behaviors.sidekickAjax = {
        attach: function (context, settings) {
            let ajax = null;
            ajax = findAjax();
            if (ajax) {
                ajax.success = function (response, status) {
                    Drupal.Ajax.prototype.success.call(this, response, status);
                }
                ajax.progress = false;
            }
        }
    };

    Drupal.behaviors.sidekickMessaging = {
        attach: function (context, settings) {
            const $context = $(context);
            let $form = $context.find('form.sidekick-messaging-form');
            if ($form.length === 0) {
                $form = $context.parents('form.sidekick-messaging-form');
            }
            if ($form.length > 0) {
                if ($context.find('.ajax-new-content').length > 0) {
                    if (!Drupal.ajax.instances.some(isAjaxing)) {
                        triggerAjax();
                    }
                }
                const inputs = $form.find(':input, [contenteditable="true"]')
                    .not('button, input[type="button"]');
                $(once('sidekick-messaging-form', inputs)).on(
                    'change paste keyup',
                    function (event) {
                        var $form = $(event.target).parents('.sidekick-messaging-form').first();
                        if ($form.length) {
                            if (!Drupal.ajax.instances.some(isAjaxing)) {
                                triggerAjax();
                            }
                        }
                    }
                );
            }

            $.fn.initSuggestions = function (element, suggestionsClass) {
                $(suggestionsClass + ' ul').attr('style', 'width:' + (parseInt($(element).width()) + 30) + 'px;');
                $(suggestionsClass + ' ul li a').each(function () {
                    $(this).mouseenter(function () {
                        $(this).addClass('ui-state-active');
                    }).mouseleave(function () {
                        $(this).removeClass('ui-state-active');
                    });
                    $(this).click(function () {
                        var finaltext = $(this).text();
                        finaltext = finaltext.replace(/\d+\./g, '');
                        $(element).val(finaltext);
                        $(suggestionsClass).hide();
                        $(suggestionsClass).html('');
                    });
                });

                $(element).focus();

                var li = $(suggestionsClass + ' ul li');
                var liSelected;
                var next = '';
                if (li.length > 0) {
                    $(window).keydown(function (e) {
                        if (e.which === 40) {
                            if (liSelected) {
                                $(liSelected).find('a').removeClass('ui-state-active');
                                next = liSelected.next();
                                if (next.length > 0) {
                                    liSelected = next;
                                    $(liSelected).find('a').addClass('ui-state-active');
                                } else {
                                    liSelected = li.eq(0);
                                    $(liSelected).find('a').addClass('ui-state-active');
                                }
                            } else {
                                liSelected = li.eq(0);
                                $(liSelected).find('a').addClass('ui-state-active');
                            }
                        } else if (e.which === 38) {
                            if (liSelected) {
                                $(liSelected).find('a').removeClass('ui-state-active');
                                next = liSelected.prev();
                                if (next.length > 0) {
                                    liSelected = next;
                                    $(liSelected).find('a').addClass('ui-state-active');
                                } else {
                                    liSelected = li.last();
                                    $(liSelected).find('a').addClass('ui-state-active');
                                }
                            } else {
                                liSelected = li.last();
                                $(liSelected).find('a').addClass('ui-state-active');
                            }
                        } else if (e.which === 13) {
                            var finaltext = $(liSelected).text();
                            liSelected = '';
                            finaltext = finaltext.replace(/\d+\./g, '');
                            $(element).val(finaltext);
                            $(suggestionsClass).hide();
                            $(suggestionsClass).html('');
                            e.preventDefault();
                        }
                    });
                }
            };
        }
    };

    function findAjax() {
        let ajax = null;
        const selector = '#sidekick-messaging-button';
        for (const index in Drupal.ajax.instances) {
            if (Drupal.ajax.instances.hasOwnProperty(index)) {
                const ajaxInstance = Drupal.ajax.instances[index];
                if (ajaxInstance && (ajaxInstance.selector === selector)) {
                    ajax = ajaxInstance;
                    break;
                }
            }
        }
        return ajax;
    }

    function triggerAjax() {
        let ajax;
        ajax = findAjax();
        if (ajax) {
            $(ajax.element).trigger(ajax.element_settings ? ajax.element_settings.event : ajax.elementSettings.event);
        }
        if (drupalSettings.sidekick.sidekickData) {
            const targetIframe = $('.sidekick-frame iframe', context).content;
            if (!!targetIframe) {
                targetIframe.contentWindow.postMessage(drupalSettings.sidekick.sidekickData, '*');
            }
        }
    }

    function isAjaxing(instance) {
        return instance && instance.hasOwnProperty('ajaxing') && instance.ajaxing === true;
    }

    document.addEventListener("DOMContentLoaded", function () {
        window.selectSuggestion = function(element, value) {
            $(element).val(value)
            $(element).siblings('.custom-autocomplete').remove();
        };
    });

}(jQuery, Drupal, drupalSettings));
